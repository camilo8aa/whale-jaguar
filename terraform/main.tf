terraform {
  backend "remote" {
    hostname      = "app.terraform.io"
    organization  = "flypass"

    workspaces {
      name = "aws_fiona_pdn"
    }
  }
}
