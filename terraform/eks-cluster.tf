#
# EKS Cluster Resources
#  * IAM Role to allow EKS service to manage other AWS services
#  * EC2 Security Group to allow networking traffic with EKS cluster
#  * EKS Cluster
#

resource "aws_iam_role" "rl-eks-cluster-pdn" {
  name = "rl-eks-cluster-pdn"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY

}

resource "aws_iam_role_policy_attachment" "eks-cluster-pdn-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.rl-eks-cluster-pdn.name
}

resource "aws_iam_role_policy_attachment" "eks-cluster-pdn-AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.rl-eks-cluster-pdn.name
}

resource "aws_security_group" "sg-eks-cluster-pdn" {
  name        = "SG-eks-cluster-pdn"
  description = "Cluster communication with worker nodes"
  vpc_id      = aws_vpc.eks-cluster-pdn.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "eks-cluster-pdn"
  }
}

resource "aws_security_group_rule" "eks-cluster-pdn-ingress-workstation-https" {
  # TF-UPGRADE-TODO: In Terraform v0.10 and earlier, it was sometimes necessary to
  # force an interpolation expression to be interpreted as a list by wrapping it
  # in an extra set of list brackets. That form was supported for compatibility in
  # v0.11, but is no longer supported in Terraform v0.12.
  #
  # If the expression in the following list itself returns a list, remove the
  # brackets to avoid interpretation as a list of lists. If the expression
  # returns a single list item then leave it as-is and remove this TODO comment.
  cidr_blocks       = [local.workstation-external-cidr]
  description       = "Allow workstation to communicate with the cluster API Server"
  from_port         = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.sg-eks-cluster-pdn.id
  to_port           = 443
  type              = "ingress"
}

resource "aws_eks_cluster" "eks-cluster-pdn" {
  name     = var.cluster-name
  role_arn = aws_iam_role.rl-eks-cluster-pdn.arn

  vpc_config {
    security_group_ids = [aws_security_group.sg-eks-cluster-pdn.id]

    # subnet_ids         = "${aws_subnet.demo[*].id}"
    # subnet_ids = aws_subnet.demo[0].id
    subnet_ids = [aws_subnet.subnet-us-east-1a.id,aws_subnet.subnet-us-east-1b.id]
  }

  depends_on = [
    aws_iam_role_policy_attachment.eks-cluster-pdn-AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.eks-cluster-pdn-AmazonEKSServicePolicy,
  ]
}