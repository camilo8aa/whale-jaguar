#
# VPC Resources
#  * VPC
#  * Subnets
#  * Internet Gateway
#  * Route Table
#

resource "aws_vpc" "eks-cluster-pdn" {
  cidr_block = "172.80.0.0/16"
  enable_dns_hostnames = true

  tags = {
    "Name"                                      = "eks-cluster-pdn"
    "kubernetes.io/cluster/${var.cluster-name}" = "shared"
  }
}

resource "aws_subnet" "subnet-us-east-1a" {
  # count = 2

  availability_zone = "us-east-1a"
  cidr_block        = "172.80.1.0/24"
  vpc_id            = aws_vpc.eks-cluster-pdn.id
  map_public_ip_on_launch = true

  tags = {
    "Name"                                      = "eks-cluster-pdn"
    "kubernetes.io/cluster/${var.cluster-name}" = "shared"
  }
}

resource "aws_subnet" "subnet-us-east-1b" {
  # count = 2

  availability_zone = "us-east-1b"
  cidr_block        = "172.80.2.0/24"
  vpc_id            = aws_vpc.eks-cluster-pdn.id
  map_public_ip_on_launch = true

  tags = {
    "Name"                                      = "eks-cluster-pdn"
    "kubernetes.io/cluster/${var.cluster-name}" = "shared"
  }
}

resource "aws_internet_gateway" "ig-eks-cluster-pdn" {
  vpc_id = aws_vpc.eks-cluster-pdn.id

  tags = {
    Name = "eks-cluster-pdn"
  }
}

resource "aws_route_table" "rt-eks-cluster-pdn" {
  vpc_id = aws_vpc.eks-cluster-pdn.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.ig-eks-cluster-pdn.id
  }
}

resource "aws_route_table_association" "eks-cluster-pdn1" {

  subnet_id      = aws_subnet.subnet-us-east-1a.id
  route_table_id = aws_route_table.rt-eks-cluster-pdn.id
}

resource "aws_route_table_association" "eks-cluster-pdn2" {

  subnet_id      = aws_subnet.subnet-us-east-1b.id
  route_table_id = aws_route_table.rt-eks-cluster-pdn.id
}