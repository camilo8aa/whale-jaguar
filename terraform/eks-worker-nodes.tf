#
# EKS Worker Nodes Resources
#  * IAM role allowing Kubernetes actions to access other AWS services
#  * EKS Node Group to launch worker nodes
#

resource "aws_iam_role" "node-eks-cluster-pdn" {
  name = "terraform-eks-node-eks-cluster-pdn"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY

}

resource "aws_iam_role_policy_attachment" "node-eks-cluster-pdn-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.node-eks-cluster-pdn.name
}

resource "aws_iam_role_policy_attachment" "node-eks-cluster-pdn-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.node-eks-cluster-pdn.name
}

resource "aws_iam_role_policy_attachment" "node-eks-cluster-pdn-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.node-eks-cluster-pdn.name
}

resource "aws_eks_node_group" "node-eks-cluster-pdn" {
  cluster_name    = aws_eks_cluster.eks-cluster-pdn.name
  node_group_name = "node-eks-cluster-pdn"
  node_role_arn   = aws_iam_role.node-eks-cluster-pdn.arn
  instance_types  = ["t3.small"]
  disk_size = 70

  # subnet_ids      = "${aws_subnet.demo[*].id}"
  subnet_ids = [aws_subnet.subnet-us-east-1a.id,aws_subnet.subnet-us-east-1b.id]

  scaling_config {
    desired_size = 1
    max_size     = 1
    min_size     = 1
  }

  depends_on = [
    aws_iam_role_policy_attachment.node-eks-cluster-pdn-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.node-eks-cluster-pdn-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.node-eks-cluster-pdn-AmazonEC2ContainerRegistryReadOnly,
  ]
}